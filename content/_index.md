---
title: "Home"
date: 
sitemap:
priority : 1.0

outputs:
- html
- css
- json
---
Actuellement en deuxième année de Master Méthodes Informatiques Appliquées à la Gestion des Entreprises (MIAGE), spécialisation Valorisation de la Donnée, à l'Université Paris Cité, j'occupe le poste de Proxy Product Owner chez Alpiq Retail France.

Tout au long de mon parcours, j'ai évolué dans le domaine de la programmation et du développement web, ce qui m'a permis d'acquérir une solide expertise technique. Désormais, je complète mes compétences en me spécialisant dans la gestion de projet informatique, combinant ainsi maîtrise technique et vision stratégique.

Ce portfolio vous offre un aperçu de mes réalisations personnelles et professionnelles. Vous y découvrirez les projets que j'ai menés ainsi que les expériences qui ont enrichi mon parcours.

J'espère que cette vitrine de mon travail saura témoigner de mon engagement et de mes compétences. Bonne découverte !