{
    "title":"ZEINA",
    "link": "http://www.zeina-apps.com/",
    "link_name": "Site Zeina APP",
    "image":"/img/zeina.webp",
    "description":"Zeina est une plateforme qui digitalise facilement les commerces en simplifiant la gestion des ventes, stocks et transactions. Lors de mon stage au Sénégal en 2023, j’ai contribué à son développement en tant que développeuse back-office VueJS, en optimisant ses fonctionnalités et son expérience utilisateur.<ul> <li> Marketplace </li><li> Site dynamique </li><li> Full-Stack </li><li> Développement </li><li> Back-Office </li></ul>",
    "tags":["Web","Digital", "VueJS", "JavaScript", "Swagger", "API", "Github"],
    "fact":""
}

Zeina est une suite logicielle complète qui digitalise la gestion des micro et petites entreprises du secteur informel. Elle intègre un marketplace avec système de paiement, un back-office de gestion et une offre de formation continue pour accompagner leur formalisation et professionnalisation.

Lors de mon stage au Sénégal en 2023, j’ai contribué à son développement en tant que développeuse back-office VueJS, en optimisant ses fonctionnalités pour offrir une expérience fluide et efficace aux commerçants du pays.