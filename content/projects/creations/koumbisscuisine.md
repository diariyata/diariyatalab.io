{
  "title": "KOUMBISS CUISINE",
  "image": "dev/diariyata.gitlab.io/static/img/logo-kc.png",
  "link": "https://koumbisscuisine.com/",
  "link_name": "Site Koumbiss Cuisine",  
  "image": "/img/koumbissc.webp",
  "description": "Koumbiss Cuisine est le premier site WordPress sur lequel nous avons travaillé en tant que particulier, en 2022, pour une auto-entrepreneuse. Ce site a été conçu pour développer et promouvoir son activité de traiteur à domicile, avec l'objectif d'élargir sa clientèle et de renforcer sa présence en ligne.<ul> <li> Site statique </li><li> CMS </li><li> Wordpress </li> </ul>",
  "tags": ["DevOps","HTML", "CSS", "CMS", "WORDPRESS"],
  "fact": "",
  "featured":true
}


Le site Koumbiss Cuisine, entièrement développé sur WordPress, a été conçu pour soutenir une entrepreneuse dans l’expansion de son activité de traiteur à domicile.

En collaboration avec ma partenaire principale, nous avons réalisé ce projet en 2022 pour approfondir nos compétences en développement web, en particulier en utilisant les fonctionnalités des CMS, afin de renforcer notre expertise en développement web full stack.

Ce projet nous a également permis d'acquérir une expérience précieuse en tant que prestataires pour un client réel, affinant notre capacité à comprendre et anticiper les besoins des consommateurs, une compétence essentielle pour nos futures opportunités professionnelles.