{
  "title": "HANDICAP ENFANCE INSERTION",
  "image": "dev/diariyata.gitlab.io/static/img/heei.png",
  "link": "https://garagenum.gitlab.io/stack/heei.legaragenumerique.fr",
  "link_name": "Site hébergé sur gitlab.io",  
  "image": "/img/heei.webp",
  "description": "HEEI (Handicap Enfance Insertion) est le premier site en langage Go sur lequel j'ai travaillé en 2020. Entièrement statique, il a été généré avec la plateforme Hugo et conçu pour une association partenaire œuvrant en faveur de l’éducation des enfants handicapés en Afrique de l’Ouest. <ul> <li> Site statique </li><li> Blog  </li><li> Front-End </li> <li> Back-End </li> </ul>",
  "tags": ["DevOps","HUGO", "JavaScript", "HTML", "CSS", "Markdown", "Gitlab"],
  "fact": "",
  "featured":true
}


Le site HEEI (Handicap Enfance Insertion) est le premier site en Go sur lequel j’ai travaillé en 2020. Développé avec la plateforme Hugo, il offre une solution entièrement statique. 

Destiné à une association engagée dans l’éducation des enfants handicapés en Afrique de l’Ouest, ce projet a été réalisé en collaboration avec l’équipe du Garage Numérique, déjà investie auprès de Handicap Enfance Insertion. L’objectif était d’améliorer la visibilité de l’association et d’élargir son réseau de contacts et d'adhérents.

J’ai contribué à ce projet lors de mon service civique au Garage Numérique, bénéficiant d’un encadrement précieux de mes supérieurs.
