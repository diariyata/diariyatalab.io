{
    "title":"HBH COSMETICS",
    "link": "https://hbh.legaragenumerique.eu",
    "link_name": "Site hébergée sur dokku",
    "image":"/img/hbhicon.webp",
    "description":"HBH COSMETICS est un site e-commerce dynamique, en cours de réalisation, sur lequel nous avons travaillé avec la plateforme Hugo. Conçu pour aider notre cliente à étendre son activité, il vise à toucher un public plus large grâce à une boutique en ligne et à la promotion de ses produits et services.<ul> <li> Site e-commerce</li><li> Gestion de paiement  </li><li> Full-Stack </li><li> Base de données </li><li> Back-Office </li></ul>",
    "tags":["DevOps","HUGO", "JavaScript", "HTML", "CSS", "Markdown", "Gitlab", "Stripes", "API"],
    "fact":""
}

HBH COSMETICS est un projet de site e-commerce en cours de développement, conçu avec la plateforme Hugo. Son objectif est d’accompagner notre cliente dans l’expansion de son activité en proposant une boutique en ligne optimisée et en renforçant la visibilité de ses produits et services.

Nous travaillons à intégrer des fonctionnalités avancées, telles que la gestion des commandes, la livraison à domicile et la promotion sur les réseaux sociaux. Le défi principal est d’exploiter Hugo, habituellement utilisé pour des sites statiques, afin de créer une plateforme e-commerce dynamique avec un système de gestion des paiements en ligne.