{
    "title":"BLOODY FAM",
    "link": "https://bloodyfam.com/",
    "link_name": "Site Bloody Fam",
    "image":"/img/homepage.webp",
    "description":"Bloody Fam est un site dynamique conçu sur Shopify pour promouvoir la marque de prêt-à-porter d'un jeune créateur de mode parisien. Ce projet vise à offrir une plateforme en ligne moderne et fonctionnelle, mettant en avant les créations uniques du créateur et facilitant l’expérience d'achat pour les consommateurs.<ul> <li>Shopify</li><li> E-commerce</li><li>Dynamique</li></ul>",
    "tags":["Shopify", "CMS", "E-commerce", "DevOps"],
    "fact":""
}


Bloody Fam est un projet personnel que j’ai mené avec ma collaboratrice pour nous initier au CMS Shopify. Ce site dynamique a été développé indépendamment dans le but de promouvoir et d’élargir la portée de la marque de prêt-à-porter d’un jeune créateur de mode parisien. En choisissant Shopify comme plateforme, nous avons créé une vitrine en ligne attrayante et fonctionnelle, permettant de présenter les créations de la marque et d'attirer un public plus large.
